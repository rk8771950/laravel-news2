<?php


use App\Http\Controllers\AdminController;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\UserAuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/index', function () {
//     return view('index');
// })->name('index');
// Route::get('/login', function () {
//     return view('auth.login');
// })->name('login');
// route::prefix('cms/admin')->middleware('guest:admin')->group(function(){
    // });
Route::resource('admins', AdminController::class);

// Route::get('{guard}/login',[UserAuthController::class,'showLogin'])->middleware('guest:admin')->name('login.viwe');
// Route::post('{guard}/login',[UserAuthController::class,'Login'])->middleware('guest:admin');


Route::get('index', [CustomAuthController::class, 'index2'])->name('index');
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registration', [CustomAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');