<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articals', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('short_description');
            $table->string('full_description');
            $table->foreignId('category_id');
            $table->foreign('category_id')->on('categories')->references('id');
            $table->foreignId('auther_id');
            $table->foreign('auther_id')->on('authers')->references('id');
            $table->string('seen_count');
            $table->foreignId('images_id');
            $table->foreign('images_id')->on('images')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articals');
    }
}
