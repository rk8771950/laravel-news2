<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable=['*'];
    public function subCategory()
    {
        return $this->belongsTo(subCategory::class,)
    }
    public function productOrders()
    {
        return $this->hasMany(OrderProduct::class,)
    }
    public function images()
    {
        return $this->morphMany(Image::class,'object','object_type','object_id','id')
    }
    public function image()
    {
        return $this->morphOne(Image::class,'object','object_type','object_id','id')
    }
    public function orders()
    {
        // return $this->belongsToMany(Order::class,OrderProduct::class,'product_id','order_id')
        return $this->belongsToMany(Order::class,'order_products')
    }
}
