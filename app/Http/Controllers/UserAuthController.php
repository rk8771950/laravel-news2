<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserAuthController extends Controller
{
    public function showLogin($guard){
        return response()->view('auth.login',compact('guard'));
    }
    public function login(){

    }
    public function logout(){

    }
}
